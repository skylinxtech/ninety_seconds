RSpec.configure do |config|
  config.append_after(:each) do
    connection[:projects].drop
    connection[:users].drop
  end
end
