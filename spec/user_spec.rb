require 'spec_helper'

describe NinetySeconds::User do
  describe '#find' do
    context 'When you find data of the user' do
      context 'with cities that exists in our db' do
        let(:caller) { described_class.search('admin') }
        it 'should return matched documents' do
          expect(caller.count).to be_equal(1)
        end
      end
    end
  end
end
