module BaseObjects
  extend RSpec::SharedContext
  let(:connection) do
    Mongo::Client.new(['localhost:27017'], database: 'ninty_seconds_test')
  end
  let(:user) do
    connection[:users].insert_one(
      _id: 3,
      external_id: '74341f74-9c79-49d5-9611-87ef9b6eb75f',
      name: 'Raylan Givens',
      created_at: '2016-04-15T05:19:46 -10:00',
      active: true,
      verified: true,
      shared: false,
      locale: 'en-AU',
      timezone: 'Sri Lanka',
      last_login_at: '2013-08-04T01:03:27 -10:00',
      email: 'test@example.com',
      phone: '8335-422-718',
      organization_id: 119,
      tags: [
        'Springville',
        'Sutton',
        'Hartsville/Hartley',
        'Diaperville'
      ],
      suspended: true,
      role: 'admin'
    )
  end

  let(:project) do
    connection[:projects].insert_one(
      _id: 101,
      url: 'http://example.com/projects-1',
      external_id: '9270ed79-35eb-4a38-a46f-35725197ea8d',
      name: 'My first project',
      tags: [
        'Exterior',
        'Workplace Fun'
      ],
      created_at: '2016-05-21T11:10:28 -10:00'
    )
  end
end
RSpec.configure { |c| c.include BaseObjects }
