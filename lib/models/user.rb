require 'mongo'
module NinetySeconds
  # user model
  class User < Base
    class << self
      def search_params(term)
        {
          '$or': [
            { name: term }, { email: term },
            { locale: term },
            { timezone: term },
            { phone: term },
            { role: term },
            { tags: { '$all': [term] } }
          ]
        }
      end

      def cols
        %i[
          name external_id created_at active verified shared locale
          timezone last_login_at email phone organization_id suspended
          role tags
        ]
      end

      def search(search_term)
        find(search_params(search_term), cols)
      end
    end
  end
end
