require 'mongo'
module NinetySeconds
  # project model
  class Project < Base
    class << self
      def search_params(term)
        {
          '$or': [
            { url: term }, { name: term },
            { details: term },
            { external_id: term },
            { tags: { '$all': [term] } },
            { location: { '$all': [term] } }
          ]
        }
      end

      def cols
        %i[
          name url details created_at tags location
        ]
      end

      def search(search_term)
        find(search_params(search_term), cols)
      end
    end
  end
end
