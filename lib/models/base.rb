module NinetySeconds
  # it will provide base class functions for all models
  class Base

    @@page = 0
    @@per_page = 10

    class << self

      def data_not_found
        p '====================='
        p "No #{collection_name} Found matching with your query"
        p '=====================\n'
      end

      def ask_for_more
        p 'Do you want to view more? (y/n)'
        %w[y Y].include?(gets.strip)
      end

      def client
        @client ||= Database.instance.connection
      end

      def collection
        client[collection_name]
      end

      def collection_name
        @collection_name ||= class_name.downcase.pluralize.to_sym
      end

      def class_name
        @class_name = name.gsub(/^.*::/, '')
      end

      def create(params = {})
        raise InvalidInput if params.empty? || params.instance_of?(Array)
        collection.insert_one(params)
      end

      def increment_page
        @@page += 1
      end

      def new_count
        @@per_page * @@page
      end

      def check_for_more_pages(conditions, cols, data)
        return if data.count < new_count
        return unless ask_for_more
        find(conditions, cols)
      end

      def query(conditions)
        collection.find(conditions)
                  .limit(@@per_page)
                  .skip(@@page * @@per_page)
      end

      def find(conditions, cols)
        data = query(conditions)
        return data_not_found if data.count.zero?
        Print.show(data, cols)
        # tp(data, cols)
        increment_page
        check_for_more_pages(conditions, cols, data)
      end
    end
  end
end
