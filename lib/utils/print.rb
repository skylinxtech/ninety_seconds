module NinetySeconds
  # it will be used to print data output
  class Print
    class << self
      def show(data, cols)
        data.each do |entry|
          p '==========================================='
          cols.each do |col|
            value = entry[col]
            next if value.nil?
            value = entry[col].join(',') if value.instance_of?(Array)
            p "#{col}: #{value}"
          end
          p '==========================================='
        end
      end
    end
  end
end
