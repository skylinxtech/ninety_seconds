module NinetySeconds
  # it will manage all exceptions
  class Manager
    attr_reader :exception

    def initialize(exception)
      @exception = exception
      display
    end

    def display
      if exception.instance_of?(InvalidInput)
        p exception.message || 'Invalid Input'
      else
        p exception
      end
    end
  end
end
