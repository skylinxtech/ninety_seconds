module NinetySeconds
  # it will be used as a parent to custom exceptions
  class CustomException < StandardError
    attr_reader :message
    def initialize(message = nil)
      @message = message
    end
  end
end
