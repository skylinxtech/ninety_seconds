module NinetySeconds
  # it will be used to bootstap all operation
  class Bootstrap
    def initialize
      init
    end

    private

    def init
      welcome_message
      check_for_user_action
    end

    def welcome_message
      p 'What you are planning for today?'
      p '1. Seed sample data'
      p '2. Search users'
      p '3. Search projects'
      p '4. Quit'
    end

    def check_for_user_action
      input = gets.strip.to_i
      return send(actions[input]) if actions.key?(input)
      raise InvalidInput
    end

    def actions
      @actions ||= {
        1 => 'seed',
        2 => 'search_user',
        3 => 'search_project',
        4 => 'quit'
      }
    end

    def seed
      Database.instance.seed
      init
    end

    def search_user
      p 'Please enter your search terms (i.e. Name, Email e.t.c)'
      search_term = gets.strip
      User.search(search_term)
      init
    end

    def search_project
      p 'Please enter your search terms (i.e. Name, Email e.t.c)'
      search_term = gets.strip
      Project.search(search_term)
      init
    end

    def quit
      exit
    end
  end
end
