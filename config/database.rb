module NinetySeconds
  # it will be used to handle database related operations
  class Database
    # the class variable @@instance holds
    # a reference to the singleton db object.
    @@instance = Database.new

    # the class #instance method returns the
    # single reference to the db instance.
    def self.instance
      @@instance
    end

    def connection
      @connection ||= Mongo::Client.new(
        ["#{ENV['HOST']}:#{ENV['PORT']}"], database: ENV['DATABASE']
      )
    end

    def create_indexes
      connection[:projects].indexes.create_one('$**': 'text')
      connection[:users].indexes.create_one('$**': 'text')
    end

    def file_collection(file)
      file.split('/').last.gsub('.json', '').to_sym
    end

    def process_data(data, collection)
      if data.instance_of?(Array)
        connection[collection].insert_many(data)
      else
        connection[collection].insert_one(data)
      end
    end

    def seed
      create_indexes
      Dir.glob('data/*.json').each do |data_file|
        data = JSON.parse(File.read(data_file))
        process_data(data, file_collection(data_file))
      end
    end

    private_class_method :new
  end
end
