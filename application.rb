require 'dotenv/load'
require 'mongo'
require 'pry'
require 'json'
require 'active_support/inflector'
require_relative 'config/bootstrap'
require_relative 'config/base_classes'
require_relative 'config/database'
require 'table_print'
Dir.glob('lib/models/*.rb').each { |file| require_relative file.split('.')[0] }

# it will be used to initialize application
module NinetySeconds
  # it will be use to initialize application
  class Application
    def initialize
      Bootstrap.new
    rescue StandardError => e
      Manager.new(e)
    end
  end
end

NinetySeconds::Application.new
