## A solutios to help users to search projects or users based on keyword

## Overview

This solution will demonstrate an efficient way of searching users or projects using ruby cli

## Prerequisites
Before using this sample application, make sure you have a mongod instance (version 3.2 or later) running on localhost port 27017 (the default).

 * [MongoDb Installation Instructions](https://docs.mongodb.com/manual/installation/)

 Please add details of your database connection in the .env file.

## Execution

In order to run application, Please execute following command

```
  ruby application.rb
```
it will popup 4 options

1. Seed Sample data
2. Search users
3. Search projects
4. Quit

For the first time, in order to populate sample data you can seed data into the database, Seed data will be loaded from data directory.

Programme execution is divided into three main steps

### Search users
Search users expects a search term and respond with all matched entries. It is pagined to 10 entries, you can load more entries by entering "y" after each iteration.

### Search projects
Search projects expects a search term and respond with all matched entries. It is pagined to 10 entries, you can load more entries by entering "y" after each iteration.


### Quit

it will be used to quit the application

### 4. Testing
Application test specs are not covered as it requires a complete test setup since I am using mongodb with ruby.

